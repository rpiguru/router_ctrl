import os
import threading
import traceback

import utils.common
import time
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty
from kivymd.snackbar import Snackbar
from kivy.clock import Clock, mainthread
from screens.menu.sections.base import BaseSection
import utils.config_util
from utils.common import get_system_number, set_system_number
from utils.ssh_util import RouterHacker
from widgets.dialog import LoadingMessageBox

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'connection_setting.kv'))


class ConnectionSettingSection(BaseSection):

    prev_section = StringProperty('welcome_section')
    next_section = StringProperty('device_section')
    title = StringProperty('Device Connection')
    dev_type = None
    loading_dlg = None
    router_hacker = None

    def __init__(self, **kwargs):
        super(ConnectionSettingSection, self).__init__(**kwargs)
        self.loading_dlg = LoadingMessageBox(size_hint_x=.6)

    def on_enter(self, *args):
        self.ids.system_number.text = get_system_number()
        self.ids.btn_next.disabled = True

    def go_next(self):
        if self.dev_type is None:
            Snackbar(text='Please select device type.', background_color=(.8, .3, 0, .5)).show()
            return
        elif self.dev_type == 'Wireless' and not self.is_connected:
            Snackbar(text='Please change the system number of Built-in router.', background_color=(.8, .3, 0, .5)).show()
            return
        super(ConnectionSettingSection, self).go_next()

    def update_system_number(self):
        val = self.ids.system_number.text
        if len(val) == 0:
            Snackbar(text='Invalid System Number', background_color=(.8, .3, 0, .5)).show()
            return
        set_system_number(val)
        Snackbar(text='System Number is updated').show()

    def on_press_check(self, chk, label):
        if chk.get_value():
            if label == 'wired':
                self.ids.btn_wireless.disabled = True
                self.ids.system_number.disabled = True
                self.en_dev_chk(True)
            elif label == 'wireless':
                self.ids.btn_wireless.disabled = False
                self.ids.system_number.disabled = False
                self.en_dev_chk(False)
            else:
                for _key in ['trolley', 'hook', 'antenna_box']:
                    self.ids['btn_{}'.format(_key)].disabled = False if label == _key else True
            # self.ids.btn_wired.disabled = True if chk.text == 'Wireless' else False
            self.dev_type = chk.text
        else:
            if label in ['wired', 'wireless']:
                self.en_dev_chk(False)
            else:
                self.ids['btn_{}'.format(label)].disabled = True

    def en_dev_chk(self, val):
        for _key in ['trolley', 'hook', 'antenna_box']:
            self.ids['chk_{}'.format(_key)].disabled = not val
            self.ids['chk_{}'.format(_key)].set_value(False)
            self.ids['btn_{}'.format(_key)].disabled = True

    def on_test_wireless(self):
        self.loading_dlg.title = 'Connecting to the Built-in Router...'
        self.loading_dlg.open()
        threading.Thread(target=self.connect_b_router).start()

    def connect_b_router(self, *args):
        """
        Check connection of built-in router
        """
        print 'Starting connection to B-Router...'
        credential = utils.config_util.get_section('b_router')
        self.router_hacker = RouterHacker(host=credential['ip'], port=int(credential['port']),
                                          model=credential['model'],
                                          username=credential['username'], password=credential['password'])
        # state, result = True, 'OK'
        # time.sleep(5)
        state, result = self.router_hacker.connect()

        if not state:
            self.on_connect_b(state=state, reason=result)
        else:
            try:
                detail = self.router_hacker.get_detail()
                print 'Successfully connected to Built-in router, detail: {}'.format(detail)
                self.on_connect_b(state=True, reason=detail)
            except Exception as e:
                # self.on_connect_b(state=True, reason={'ip_address': '192.168.1.1'})
                self.on_connect_b(state=False, reason=str(e))

    @mainthread
    def on_connect_b(self, state, reason=None):
        """
        Done callback of `connect_b_router`
        """
        self.loading_dlg.dismiss()
        if not state:
            Snackbar(text='Failed to connect to Built-in router: {}'.format(reason),
                     background_color=(.8, .3, 0, .5)).show()
        else:
            self.loading_dlg.title = 'Changing system number of Built-in Router...'
            self.loading_dlg.open()
            threading.Thread(target=self.update_b_router, args=(reason, )).start()

    def update_b_router(self, *args):
        self.loading_dlg.dismiss()
        dlg = LoadingMessageBox()
        dlg.title = 'Waiting for Built-in router to be rebooted...'
        dlg.size_hint_x = .6
        dlg.open()
        try:
            new_id = self.ids.system_number.text
            self.router_hacker.change_ssid(new_id)
            self.router_hacker.close()
            # Initialize again
            credential = utils.config_util.get_section('b_router')
            self.router_hacker = RouterHacker(host=credential['ip'], port=int(credential['port']),
                                              model=credential['model'],
                                              username=credential['username'], password=credential['password'])
            state, result = self.router_hacker.check_connection(timeout=60)
            self.on_update_b(state=state, result=result, dlg=dlg)
        except Exception as e:
            print 'Failed to update b-router: {}'.format(traceback.format_exc(e))
            self.on_update_b(state=False, result='Error', dlg=dlg)

    @mainthread
    def on_update_b(self, state, result, dlg):
        dlg.dismiss()
        if state:
            self.en_dev_chk(True)
            Snackbar(text='Successfully updated system number of Built-in router').show()
            utils.common.set_system_number(self.ids.system_number.text)
        else:
            Snackbar(text='Failed to update system number of Built-in router',
                     background_color=(.8, .3, 0, .5)).show()

    def on_test_wired(self, dev):
        self.loading_dlg.title = 'Connecting to {}...'.format(dev)
        self.loading_dlg.open()
        threading.Thread(target=self.connect_device, args=(dev, )).start()

    def connect_device(self, *args):
        dev = args[0]
        dev_num = {'Hook': '1', 'Trolley': '2', 'Antenna Box': '3'}[dev]
        credential = utils.config_util.get_section('device{}'.format(dev_num))
        self.router_hacker = RouterHacker(host=credential['ip'], port=int(credential['port']),
                                          model=credential['model'],
                                          username=credential['username'], password=credential['password'])
        state, result = self.router_hacker.check_connection(timeout=10)
        if not state:
            self.on_connected_device(dev=dev, state=state, reason=result)
        else:
            try:
                detail = self.router_hacker.get_detail()
                print 'Successfully connected to {}, detail: {}'.format(dev, detail)
                self.on_connected_device(dev=dev, state=True, reason=detail)
            except Exception as e:
                self.on_connected_device(dev=dev, state=False, reason=e.args[1])

    @mainthread
    def on_connected_device(self, dev, state, reason=None):
        self.loading_dlg.dismiss()
        if state:
            Snackbar(text='Successfully connected to {}'.format(dev)).show()
            self.ids.btn_next.disabled = False
            self.root_widget.set_cur_device(dev)
            try:
                system_number = reason['ssid_wireless'][2:]
            except KeyError:
                system_number = reason['ssid_wireless_rocket'][2:]
            utils.config_util.set_config(utils.common.get_device_section_name(dev), 'system_number', system_number)
        else:
            Snackbar(text='Failed to connect to {}: {}'.format(dev, reason),
                     background_color=(.8, .3, 0, .5)).show()

    def on_leave(self, *args):
        try:
            if self.router_hacker is not None:
                self.router_hacker.close()
        except Exception as e:
            print 'Failed to close router connection: {}'.format(e)
            pass
