import os
from kivy.clock import Clock
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, BooleanProperty

from kivy.uix.screenmanager import Screen
from kivymd.snackbar import Snackbar

from screens.settings.tabs.admin import AdminTab
import utils.config_util
from functools import partial

Builder.load_file(os.path.join(os.path.dirname(__file__), 'video.kv'))


class VideoScreen(Screen):

    title = StringProperty('Live Video')
    is_tech = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(VideoScreen, self).__init__(**kwargs)

    def on_enter(self, *args):
        super(VideoScreen, self).on_enter(*args)
        self.is_tech = True if App.get_running_app().mode == 'tech' else False
        self.ids.txt_url.disabled = not self.is_tech
        self.ids.btn_update.disabled = not self.is_tech
        url = utils.config_util.get_config('admin', 'video_url', '')
        self.ids.txt_url.text = url
        self.stop_video()
        Clock.schedule_once(partial(self.update_video_url, url))

    def update_video_url(self, url, *args):
        self.ids.vid.source = url

    def on_btn_play(self, _text):
        if _text == 'PLAY':
            Clock.schedule_once(self.play_video)
        else:
            Clock.schedule_once(self.stop_video)

    def play_video(self, *args):
        self.ids.btn_play.text = 'STOP'
        self.ids.vid.state = 'play'

    def stop_video(self, *args):
        self.ids.btn_play.text = 'PLAY'
        self.ids.vid.state = 'stop'

    def on_update(self):
        url = self.ids.txt_url.text
        utils.config_util.set_config('admin', 'video_url', url)
        Clock.schedule_once(partial(self.update_video_url, url))
        Snackbar(text='Video URL is updated').show()

    def on_leave(self, *args):
        self.stop_video()
        super(VideoScreen, self).on_leave(*args)
