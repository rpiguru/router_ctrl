from kivy.properties import BooleanProperty
from kivymd.textfields import MDTextField
from kivy.clock import Clock
from kivy.core.window import Window
from widgets.dialog import InputDialog


class HCMDTextField(MDTextField):
    ignore_clear = BooleanProperty(False)
    ignore_validate = BooleanProperty(False)
    use_dlg = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(HCMDTextField, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_dlg)

    def bind_dlg(self, *args):
        if self.use_dlg:
            self.bind(focus=self.show_dlg)

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            dlg = InputDialog(text=self.text, hint_text=self.hint_text, input_filter=self.input_filter)
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()

    def on_confirm(self, *args):
        self.text = args[1]
        self.dispatch('on_text_validate')
