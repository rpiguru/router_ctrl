"""
    Utility for configuration
"""
import ConfigParser
import os

config = ConfigParser.RawConfigParser()
config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, 'config.ini')


def get_config(section, option, default_value=None):
    """
    Get configuration value
    :param section:
    :param option:
    :param default_value:
    :return:
    """
    try:
        config.read(config_file)
        return config.get(section=section, option=option)
    except ConfigParser.NoSectionError:
        return default_value
    except ConfigParser.NoOptionError:
        return default_value


def set_config(section, option, value):
    if section not in config.sections():
        config.add_section(section=section)
    config.set(section, option, value)
    try:
        with open(config_file, 'w') as configfile:
            config.write(configfile)
        return True
    except IOError as e:
        # print('Failed to write default geo-location to file: {}'.format(e))
        return False


def get_section(section):
    config.read(config_file)
    return dict(config.items(section=section))


if get_config('general', 'debug', 'OFF').lower() == 'on':
    config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, 'config-debug.ini')
