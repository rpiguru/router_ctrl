# Service Application
===========================================================================

## Install Kivy on Raspberry Pi

https://kivy.org/docs/installation/installation-rpi.html

## Remove log messages on boot.

    sudo nano /boot/cmdline.txt

Add `logo.nologo quiet` at the end of line, and **save & exit**

    sudo nano /etc/rc.local

Add `clear` just before the line `exit 0`.

## Add custom log at boot time.

	sudo apt-get install fbi

Create logo image and name it as `splash.png`.

Copy it to `/etc/`.


Create startup script.

	sudo nano /etc/init.d/asplashscreen

And add below.

	#! /bin/sh
	### BEGIN INIT INFO
	# Provides:          asplashscreen
	# Required-Start:
	# Required-Stop:
	# Should-Start:
	# Default-Start:     S
	# Default-Stop:
	# Short-Description: Show custom splashscreen
	# Description:       Show custom splashscreen
	### END INIT INFO


	do_start () {

	    /usr/bin/fbi -T 1 -noverbose -a /etc/splash.png
	    exit 0
	}

	case "$1" in
	  start|"")
	    do_start
	    ;;
	  restart|reload|force-reload)
	    echo "Error: argument '$1' not supported" >&2
	    exit 3
	    ;;
	  stop)
	    # No-op
	    ;;
	  status)
	    exit 0
	    ;;
	  *)
	    echo "Usage: asplashscreen [start|stop]" >&2
	    exit 3
	    ;;
	esac

	:

Make script executable and install it for init mode.

	sudo chmod a+x /etc/init.d/asplashscreen
	sudo insserv /etc/init.d/asplashscreen

Now reboot.

	sudo reboot

## Disable screen saver

	sudo apt-get install x11-xserver-utils
	sudo nano /etc/X11/xinit/xinitrc

Add following at the end of the file.

	xset s off         # don't activate screensaver
	xset -dpms         # disable DPMS (Energy Star) features.
	xset s noblank     # don't blank the video device

Change another file.

	sudo nano /etc/lightdm/lightdm.conf

In the `SeatDefaults` section it gives the command for starting the X server which I modified to get it to turn off the screen saver as well as dpms.

	[SeatDefaults]
	xserver-command=X -s 0 -dpms

And reboot.

	sudo reboot


## Install GUI app

    sudo apt-get install libffi-dev libssl-dev
    cd ~
    git clone https://bitbucket.org/rpi_guru/kivymd
    cd kivymd
    sudo python setup.py install

    cd ~
    mkdir tmp
    cd tmp
    wget https://pypi.python.org/packages/48/69/d87c60746b393309ca30761f8e2b49473d43450b150cb08f3c6df5c11be5/appdirs-1.4.3.tar.gz
    tar xvzf appdirs-1.4.3.tar.gz
    cd appdirs-1.4.3
    sudo python setup.py install

    cd ..
    wget https://freefr.dl.sourceforge.net/project/pyparsing/pyparsing/pyparsing-2.2.0/pyparsing-2.2.0.tar.gz
    tar xvzf pyparsing-2.2.0.tar.gz
    cd pyparsing-2.2.0
    sudo python setup.py install

    cd ~
    git clone https://bitbucket.org/rpi_guru/router_ctrl
    cd router_ctrl
    sudo pip install -r requirements.txt
    sudo python main.py

## Enable auto-start

    sudo nano /etc/rc.local

  Add below before `exit 0`

    (cd /home/pi/router_ctrl; python main.py)&
