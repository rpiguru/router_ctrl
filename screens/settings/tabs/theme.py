import os
from kivy.lang import Builder
from kivymd.theme_picker import MDThemePicker
from screens.settings.tabs.base import BaseTab
from kivy.clock import Clock

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'theme.kv'))


class ThemeTab(BaseTab):

    @staticmethod
    def on_btn():
        Clock.schedule_once(MDThemePicker().open)
