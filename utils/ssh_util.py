import os
import pprint
import paramiko.ssh_exception
import paramiko
import time
import utils.config_util

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
paramiko.util.log_to_file(cur_dir + 'router_hacker.log')

name_map = {
    'ssid_wired': 'aaa.1.ssid',
    'ssid_wireless': 'wpasupplicant.profile.1.network.1.ssid',
    'ssid_wireless_rocket': 'wireless.1.ssid',
    'device_name': 'resolv.host.1.name',
    'ip_address': 'dhcpc.1.fallback',
    'username': 'users.1.name',
    'password': 'users.1.password',
}


class RouterHacker:

    host = ''
    port = 22
    username = ''
    password = ''
    model = ''
    r_type = 'AP'
    s = None
    info = {}

    def __init__(self, host='10.1.10.136', port=22, username='', password='', model='', router_type='AP'):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.model = model
        self.r_type = 'AP'
        self._name_map = name_map
        self.s = paramiko.SSHClient()

    def connect(self):
        try:
            print 'SSH Util: Connecting to the router: {}'.format(self.host)
            self.s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.s.connect(self.host, self.port, self.username, self.password, timeout=5)
            print 'SSH Util: Connected to {}'.format(self.host)
            return True, self.get_detail()
        except paramiko.ssh_exception.AuthenticationException:
            # print 'Failed to authenticate, please check username & password'
            return False, 'Failed to login'
        except paramiko.ssh_exception.NoValidConnectionsError:
            # print 'Cannot connect to the host, please check IP address & port'
            return False, 'Host not found'
        except paramiko.ssh_exception.SSHException as e:
            return False, str(e)
        except Exception as e:
            print 'SSH Util: unexpected error while connecting: {}'.format(e)
            return False, str(e)

    def get_detail(self):
        self.info = {}
        stdin, stdout, stderr = self.s.exec_command('cat /tmp/system.cfg')
        response = stdout.read()
        for line in str(response).splitlines():
            for _key in self._name_map.keys():
                if line.startswith(self._name_map[_key]) and line[len(self._name_map[_key])] == '=':
                    self.info[_key] = line.split('=')[1]
                    break
        return self.info

    def close(self):
        self.s.close()

    def update_params(self, new_data):
        self.s.exec_command('cd /tmp')
        for _key in new_data.keys():
            if _key in self.info.keys():
                cmd = "sed -i 's/{}={}/{}={}/g' /tmp/system.cfg".format(self._name_map[_key],
                                                                        self.info[_key],
                                                                        self._name_map[_key],
                                                                        new_data[_key])
                print 'CMD: {}'.format(cmd)
                self.s.exec_command(cmd)
        # save it to flash
        print 'Saving new configurations to flash...'
        stdin, stdout, stderr = self.s.exec_command('cfgmtd -f /tmp/system.cfg -w')
        response = stdout.read()
        print response
        # reboot router
        print '\nRebooting router'
        self.s.exec_command('reboot')

    def check_connection(self, timeout=20):
        print 'SSH Util: Checking connection...'
        s_time = time.time()
        try:
            self.close()
        except:
            pass

        while True:
            connect_state, result = self.connect()
            if connect_state:
                return True, self.get_detail()
            else:
                if time.time() - s_time > timeout:
                    return False, 'Connection timeout'
                else:
                    time.sleep(1)

    def change_ssid(self, new_id):
        new_data = {
            'ssid_wired': 'hc{}'.format(new_id),
            'ssid_wireless': 'hc{}'.format(new_id),
            'ssid_wireless_rocket': 'hc{}'.format(new_id),
        }
        self.update_params(new_data)


if __name__ == '__main__':

    dev_name = 'Hook'
    # dev_name = 'Trolley'
    # dev_name = 'Antenna Box'

    section = utils.common.get_device_section_name(dev_name)
    credential = utils.config_util.get_section(section)
    credential = utils.config_util.get_section('b_router')
    client = RouterHacker(host=credential['ip'], port=int(credential['port']), model=credential['model'],
                          username=credential['username'], password=credential['password'],
                          router_type=credential['type'])
    state, detail = client.connect()
    print 'Connect: ', state
    pprint.pprint(detail)
    client.change_ssid(120)
