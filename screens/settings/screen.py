import os
from kivy.clock import Clock
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ObjectProperty, BooleanProperty

from kivy.uix.screenmanager import Screen

from screens.settings.tabs.admin import AdminTab

Builder.load_file(os.path.join(os.path.dirname(__file__), 'settings.kv'))


class SettingsScreen(Screen):

    title = StringProperty('Settings')
    admin_tab = ObjectProperty(None)
    is_admin = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(SettingsScreen, self).__init__(**kwargs)
        self.admin_tab = AdminTab(id='tab_admin', root_widget=self)

    def on_enter(self, *args):
        super(SettingsScreen, self).on_enter(*args)
        Clock.schedule_once(self.update_tab_state)

    def update_tab_state(self, *args):
        mode = App.get_running_app().mode
        if mode == 'user':
            if self.is_admin:
                self.ids['tab_panel'].remove_widget(self.admin_tab)
                self.is_admin = False
        else:
            if not self.is_admin:
                self.ids['tab_panel'].add_widget(self.admin_tab)
                self.is_admin = True
