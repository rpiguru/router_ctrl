"""

    Common utilities

"""
from utils import config_util


def is_debug_mode():
    cur_mode = config_util.get_config('general', 'debug', 'OFF')
    is_debug = True if cur_mode.lower() == 'on' else False
    return is_debug


def set_system_number(val):
    config_util.set_config('b_router', 'system_number', val)


def get_system_number():
    return config_util.get_config('b_router', 'system_number', '')


def get_device_detail(index):
    return config_util.get_section('device{}'.format(index))


def get_device_section_name(dev_name):
    dev_num = {'Hook': 1, 'Trolley': 2, 'Antenna Box': 3}[dev_name]
    return 'device{}'.format(dev_num)
