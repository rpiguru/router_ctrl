import os

from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen, NoTransition, SlideTransition

from screens.menu.sections.connection_setting import ConnectionSettingSection
from screens.menu.sections.device import DeviceSection
from screens.menu.sections.welcome import WelcomeSection

Builder.load_file(os.path.join(os.path.dirname(__file__), 'menu.kv'))


class MenuScreen(Screen):

    title = StringProperty('Service Application')
    current_section = StringProperty('')  # Store title of current screen
    sections = {}  # Dict of all screens

    def __init__(self, **kwargs):
        super(MenuScreen, self).__init__(**kwargs)
        self.sections = {
            'welcome_section': WelcomeSection(root_widget=self),
            'connection_setting_section': ConnectionSettingSection(root_widget=self),
            'device_section': DeviceSection(root_widget=self),
        }
        sm = self.ids.sm
        sm.transition = NoTransition()
        sm.switch_to(self.sections['welcome_section'])
        self.current_section = 'welcome_section'
        # sm.switch_to(self.sections['device_section'])
        # self.current_section = 'device_section'

    def on_enter(self, *args):
        super(MenuScreen, self).on_enter(*args)

    def on_leave(self, *args):
        super(MenuScreen, self).on_leave(*args)

    def switch_section(self, dest_section, direction):
        if dest_section == self.current_section:
            return
        if dest_section not in self.sections.keys():
            return
        sm = self.ids.sm
        sm.transition = SlideTransition()
        self.ids.lb_title.text = self.sections[dest_section].title
        sm.switch_to(self.sections[dest_section], direction=direction)
        self.current_section = dest_section

    def set_cur_device(self, dev):
        self.sections['device_section'].set_device(dev)
