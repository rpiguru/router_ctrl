import os
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))


class BaseSection(Screen):

    root_widget = ObjectProperty(None)
    prev_section = StringProperty('welcome_section')
    next_section = StringProperty('')
    title = StringProperty('')

    def go_next(self):
        self.root_widget.switch_section(self.next_section, 'left')

    def go_previous(self):
        self.root_widget.switch_section(self.prev_section, 'right')
