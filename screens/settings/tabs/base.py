import os
from kivy.lang import Builder
from kivy.properties import ObjectProperty

from kivymd.tabs import MDTab

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))


class BaseTab(MDTab):

    root_widget = ObjectProperty(None)
